export interface Customer {
  name: String;
  phone: String;
  email: String;
  user_id: number;
  id: number;
}
