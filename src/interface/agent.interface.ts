export interface Agent {
  name: String;
  phone: String;
  location: String;
  email: String;
  latitude: any;
  longitude: any;
  supplier_id: number;
  user_id: number;
  id: number;
}
