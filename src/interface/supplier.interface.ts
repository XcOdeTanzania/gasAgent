export interface Supplier {
  name: String;
  phone: String;
  location: String;
  email: String;
  latitude: any;
  longitude: any;
  user_id: number;
  id: number;
}
