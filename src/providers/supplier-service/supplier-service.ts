import {Http, Response, Headers} from "@angular/http";
import generateAPI from "../../assets/util/api";
import {Injectable} from '@angular/core';
import {Supplier} from "../../interface/supplier.interface";

/*
  Generated class for the SupplierServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SupplierServiceProvider {

  currentSupplier: Supplier[];

  constructor(public http: Http) {
    console.log('Hello MiuSupplierServiceProvider Provider');
  }

  addSupplier(supplier: any, user_id: number) {
    const body = JSON.stringify({
      name: supplier.name,
      email: supplier.email,
      phone: supplier.phone,
      location: supplier.location,
      latitude: supplier.latitude,
      longitude: supplier.longitude
    });

    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(generateAPI() + 'supplier/' + user_id, body, {headers: headers})
      .map((response: Response) => {
        this.currentSupplier = response.json().supplier
        return response.json().supplier
      });
  }

  getSuppliers() {
    return this.http.get(generateAPI() + 'suppliers')
      .map(
        (response: Response) => {
          return response.json().suppliers;
        }
      );
  }

  getSupplier(supplier_id: number) {
    return this.http.get(generateAPI() + 'supplier/' + supplier_id)
      .map(
        (response: Response) => {
          return response.json().supplier;
        }
      );
  }

  updateSupplier(supplier: any, supplier_id: number) {
    const body = JSON.stringify({
      name: supplier.name,
      email: supplier.email,
      phone: supplier.phone,
      location: supplier.location,
      latitude: supplier.latitude,
      longitude: supplier.longitude
    });
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.put(generateAPI() + 'supplier/' + supplier_id, body, {headers: headers})
      .map((response: Response) => response.json().supplier);
  }

  deleteSupplier(supplier_id: number) {
    return this.http.delete(generateAPI() + 'supplier/' + supplier_id);
  }


  public getSupplierInfo(): Supplier[] {
    console.log(this.currentSupplier)
    return this.currentSupplier;
  }

  public setSupplierInfo(userInfo) {
    this.currentSupplier = userInfo
  }
}
