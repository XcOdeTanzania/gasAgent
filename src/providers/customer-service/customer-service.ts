
import { Injectable } from '@angular/core';
import {Headers, Http, Response} from "@angular/http";
import {Customer} from "../../interface/customer.interface";
import generateAPI from "../../assets/util/api";

/*
  Generated class for the CustomerServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CustomerServiceProvider {

  currentCustomer: Customer[];

  constructor(public http: Http) {
    console.log('Hello MiuCustomerServiceProvider Provider');
  }

  addCustomer(customer: any, user_id: number) {
    const body = JSON.stringify({
      name: customer.name,
      email: customer.email,
      phone: customer.phone,
    });

    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(generateAPI() + 'customer/' + user_id, body, {headers: headers})
      .map((response: Response) => {
        this.currentCustomer = response.json().customer
        return response.json().customer
      });
  }

  getCustomers() {
    return this.http.get(generateAPI() + 'customers')
      .map(
        (response: Response) => {
          return response.json().customers;
        }
      );
  }

  getCustomer(customer_id: number) {
    return this.http.get(generateAPI() + 'customer/' + customer_id)
      .map(
        (response: Response) => {
          return response.json().customer;
        }
      );
  }

  updateCustomer(customer: any, customer_id: number) {
    const body = JSON.stringify({
      name: customer.name,
      email: customer.email,
      phone: customer.phone,
      location: customer.location,
      latitude: customer.latitude,
      longitude: customer.longitude,
      user_id: customer.user_id,
    });
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.put(generateAPI() + 'customer/' + customer_id, body, {headers: headers})
      .map((response: Response) => response.json().customer);
  }

  deleteCustomer(customer_id: number) {
    return this.http.delete(generateAPI() + 'customer/' + customer_id);
  }


  public getCustomerInfo(): Customer[] {
    console.log(this.currentCustomer)
    return this.currentCustomer;
  }

  public setCustomerInfo(customerInfo) {
    this.currentCustomer = customerInfo
  }
}
