import {Injectable} from '@angular/core';
import {Headers, Http, Response} from "@angular/http";
import generateAPI from "../../assets/util/api";

/*
  Generated class for the OrderServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderServiceProvider {

  constructor(public http: Http) {
    console.log('Hello OrderServiceProvider Provider');
  }

  addOrder(order: any, agent_id: number) {
    const body = JSON.stringify({
      weight: order.weight,
      price: order.price,
      status: "false",
      location: "Mwenge",
      latitude: "12.23233",
      longitude: "23.232323",
      customer_id: "1"
    });

    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(generateAPI() + 'order/' + agent_id, body, {headers: headers})
      .map((response: Response) => {
        return response.json().order
      });
  }
}
