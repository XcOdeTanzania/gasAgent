import {Geolocation} from '@ionic-native/geolocation';
import {Injectable} from '@angular/core';

export class Location {
  latitude: any;
  longitude: any;

  constructor(latitude: any, longitude: any) {
    this.latitude = latitude;
    this.longitude = longitude;
  }
}

/*
  Generated class for the GeoLocationServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeoLocationServiceProvider {

  currentLocation: Location;

  constructor(private geolocation: Geolocation) {
  }

  setCurrentLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.currentLocation = new Location(resp.coords.latitude, resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  getLocationInfo(): Location {
    return this.currentLocation;
  }

}
