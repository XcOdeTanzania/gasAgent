import {Injectable} from '@angular/core';
import {Headers, Http, Response} from "@angular/http";
import generateAPI from "../../assets/util/api";
import {Agent} from "../../interface/agent.interface";

/*
  Generated class for the AgentServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AgentServiceProvider {

  currentAgent: Agent[];

  constructor(public http: Http) {
    console.log('Hello MiuAgentServiceProvider Provider');
  }

  addAgent(agent: any, user_id: number) {
    const body = JSON.stringify({
      name: agent.name,
      email: agent.email,
      phone: agent.phone,
      location: agent.location,
      latitude: agent.latitude,
      longitude: agent.longitude,
      user_id: user_id
    });

    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(generateAPI() + 'agent/' + agent.supplier_id, body, {headers: headers})
      .map((response: Response) => {
        this.currentAgent = response.json().agent
        return response.json().agent
      });
  }

  getAgents() {
    return this.http.get(generateAPI() + 'agents')
      .map(
        (response: Response) => {
          return response.json().agents;
        }
      );
  }

  getAgent(agent_id: number) {
    return this.http.get(generateAPI() + 'agent/' + agent_id)
      .map(
        (response: Response) => {
          return response.json().agent;
        }
      );
  }

  updateAgent(agent: any, agent_id: number) {
    const body = JSON.stringify({
      name: agent.name,
      email: agent.email,
      phone: agent.phone,
      location: agent.location,
      latitude: agent.latitude,
      longitude: agent.longitude,
      user_id: agent.user_id,
    });
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.put(generateAPI() + 'agent/' + agent_id, body, {headers: headers})
      .map((response: Response) => response.json().agent);
  }

  deleteAgent(agent_id: number) {
    return this.http.delete(generateAPI() + 'agent/' + agent_id);
  }


  public getAgentInfo(): Agent[] {
    console.log(this.currentAgent)
    return this.currentAgent;
  }

  public setAgentInfo(agentInfo) {
    this.currentAgent = agentInfo
  }

}
