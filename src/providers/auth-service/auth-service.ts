import {Injectable} from '@angular/core';
import {Http, Response, Headers} from "@angular/http";
import generateAPI from "../../assets/util/api";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {User} from "../../interface/user.interface";

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  private isLoggedIn = false;
  currentUser: User[];

  constructor(public http: Http) {
    console.log('Hello AuthServiceProvider Provider');
  }


  public login(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      const body = JSON.stringify({
        email: credentials.email,
        password: credentials.password
      });
      const headers = new Headers({"Content-Type": "application/json"});
      return this.http.post(generateAPI() + 'login', body, {headers: headers})
        .map((response: Response) => {
          this.currentUser = response.json();
          this.isLoggedIn = true;
          return response.json();
        });
    }
  }

  public register(credentials) {

    const body = JSON.stringify({
      name: credentials.name,
      email: credentials.email,
      password: credentials.password
    });
    const headers = new Headers({"Content-Type": "application/json"});
    return this.http.post(generateAPI() + 'signup', body, {headers: headers})
      .map((response: Response) => {
        console.log(response.json());
        return response.json();
      });
  }

  public getUserInfo(): User[] {
    console.log(this.currentUser)
    return this.currentUser;
  }

  public setUserInfo(userInfo) {
    this.currentUser = userInfo
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      this.isLoggedIn = false;
      observer.next(true);
      observer.complete();
    });
  }

  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  authenticated(): boolean {
    return this.isLoggedIn;
  }

  getUserDetails(user_id) {
    const headers = new Headers({"Content-Type": "application/json"});
    return this.http.get(generateAPI() + 'user/details/' + user_id, {headers: headers})
      .map((response: Response) => {
        return response.json();
      });

  }
}
