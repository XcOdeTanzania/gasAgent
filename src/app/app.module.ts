import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Geolocation} from '@ionic-native/geolocation';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {AgentServiceProvider} from '../providers/agent-service/agent-service';
import {PaymentServiceProvider} from '../providers/payment-service/payment-service';
import {CustomerServiceProvider} from '../providers/customer-service/customer-service';
import {SupplierServiceProvider} from '../providers/supplier-service/supplier-service';
import {NotificationServiceProvider} from '../providers/notification-service/notification-service';
import {OrderServiceProvider} from '../providers/order-service/order-service';
import {HttpModule} from "@angular/http";
import {IonicStorageModule} from "@ionic/storage";
import { GeoLocationServiceProvider } from '../providers/geo-location-service/geo-location-service';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    AgentServiceProvider,
    PaymentServiceProvider,
    CustomerServiceProvider,
    SupplierServiceProvider,
    NotificationServiceProvider,
    OrderServiceProvider,
    GeoLocationServiceProvider
  ]
})
export class AppModule {


}
