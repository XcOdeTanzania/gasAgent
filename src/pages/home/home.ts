import {Component, ElementRef, ViewChild} from '@angular/core';
import {NavController} from "ionic-angular";
import {Geolocation} from '@ionic-native/geolocation';
import {AgentServiceProvider} from "../../providers/agent-service/agent-service";
import 'rxjs/add/operator/finally';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  agents: any;

  constructor(public navCtrl: NavController,
              private geolocation: Geolocation,
              public  _agentService: AgentServiceProvider) {

  }

  ionViewDidLoad() {
    this.loadMap();
    this.getAllAgents();
  }

  loadMap() {

    this.geolocation.getCurrentPosition().then((position) => {

      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      for (var i = 0; i < this.agents.length; i++)
        this.addMarker(this.agents[i]);
    }, (err) => {
      console.log(err);
    });

  }

  addMarker(agent) {
    //var  myLatLng = new google.maps.LatLng({lat: -34, lng: 151});
    var myLatLng = {lat: Number(agent.latitude), lng: Number(agent.longitude)};
    let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: myLatLng
      }
    );

    let content = "<h4>" + agent.name + " Gas Agent</h4>";

    this.addInfoWindow(marker, content, agent);

  }

  addInfoWindow(marker, content, agent) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
      this.onGasAgentTapped(agent);
    });

  }

  onGasAgentTapped(agent) {
    this.navCtrl.push('AgentPage', {
      agent: agent
    });
  }

  getAllAgents() {
    this._agentService.getAgents()
      .finally(() => {
      })
      .subscribe(agents => {
        this.agents = agents;
      })
  }
}
