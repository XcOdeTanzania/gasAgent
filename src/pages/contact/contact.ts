import {Component} from '@angular/core';
import {AlertController, NavController, ToastController} from 'ionic-angular';
import {SupplierServiceProvider} from "../../providers/supplier-service/supplier-service";
import {AgentServiceProvider} from "../../providers/agent-service/agent-service";
import {CustomerServiceProvider} from "../../providers/customer-service/customer-service";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  agent: boolean = false;
  customer: boolean = false;
  supplier: boolean = false;
  notifications = [{user_id: 1, weight: '20kg', price: '10,000'},
    {user_id: 1, weight: '20kg', price: '10,000'},
    {user_id: 1, weight: '20kg', price: '10,000'},
    {user_id: 1, weight: '20kg', price: '10,000'},
    {user_id: 1, weight: '20kg', price: '10,000'},
    {user_id: 1, weight: '20kg', price: '10,000'},]

  constructor(public navCtrl: NavController,
              private toast: ToastController,
              public alertCtrl: AlertController,
              public _supplierService: SupplierServiceProvider,
              public  _agentService: AgentServiceProvider,
              public _customerService: CustomerServiceProvider) {

  }

  ionViewDidLoad() {
    if (this._supplierService.currentSupplier != null) {
      this.supplier = true;
    } else if (this._agentService.currentAgent != null) {
      this.agent = true;
    }
  }

  onLogOut() {
    this._agentService.currentAgent = null;
    this._supplierService.currentSupplier = null;
    this.navCtrl.setRoot('LoginPage')
  }

  onViewNotification(notification) {

    let confirm = this.alertCtrl.create({
      title: 'Gas Order!',
      message: ' Juma has requested for your Gas 20kg',
      buttons: [
        {
          text: 'Decline',
          handler: () => {
            console.log('Disagree clicked');
            // this.onReplyToRequest(notification, false, true, 'Access denied!');
            //this.events.publish('user:checkNotification', 'Notification', false);
          }
        },
        {
          text: 'Accept',
          handler: () => {
            console.log('Agree clicked');
            this.navCtrl.push('PaymentPage', {});
            // this.onReplyToRequest(notification, true, true, 'Album request granted!')
            // this.events.publish('user:checkNotification', 'Notification', false);
          }
        }
      ]
    });
    confirm.present();


    // this._authService.getUserDetails(notification.requester_id)
    //   .subscribe(requester => {
    //     this.requesterName = requester.name;
    //
    //     this._albumService.getAnAlbum(notification.album_id)
    //       .subscribe(result => {
    //         this.album = result.album;
    //
    //         let confirm = this.alertCtrl.create({
    //           title: 'Album Request!',
    //           message: this.requesterName + ' has requested for your Album named ' + this.album.name,
    //           buttons: [
    //             {
    //               text: 'Decline',
    //               handler: () => {
    //                 console.log('Disagree clicked');
    //                 this.onReplyToRequest(notification, false, true, 'Access denied!');
    //                 this.events.publish('user:checkNotification', 'Notification', false);
    //               }
    //             },
    //             {
    //               text: 'Accept',
    //               handler: () => {
    //                 console.log('Agree clicked');
    //                // this.onReplyToRequest(notification, true, true, 'Album request granted!')
    //                // this.events.publish('user:checkNotification', 'Notification', false);
    //               }
    //             }
    //           ]
    //         });
    //         confirm.present();
    //       });
    //   });

  }
}
