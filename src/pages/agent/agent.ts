import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrderServiceProvider} from "../../providers/order-service/order-service";

/**
 * Generated class for the AgentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agent',
  templateUrl: 'agent.html',
})
export class AgentPage {

  backgroundImage = "assets/imgs/b.jpeg";
  orderForm: FormGroup;
  gasPrice: any;
  items = [
    {weight: '12kg', price: '10,000'},
    {weight: '20kg', price: '20,000'},
    {weight: '60kg', price: '40,000'},
    {weight: '80kg', price: '55,000'},
    {weight: '90kg', price: '70,000'},
    {weight: '120kg', price: '120,000'},
  ];

  agent: any;

  constructor(public navCtrl: NavController,
              public formBuilder: FormBuilder,
              public navParams: NavParams,
              public _orderService: OrderServiceProvider) {
    this.orderForm = formBuilder.group({
      weight: ['', Validators.required],
      price: [''],

    })

    this.agent = navParams.get('agent');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgentPage');
  }

  orderPressed() {
    console.log(this.orderForm.value)
    this._orderService.addOrder(this.orderForm.value, this.agent.id)
      .finally(() => {
        this.orderForm.reset();
        this.gasPrice = null;
      })
      .subscribe(order => {
        console.log(order);
      });
  }

  onWeightChange() {
    console.log(this.orderForm.value['weight'])
    for (let item of this.items) {
      if (item.weight == this.orderForm.value['weight']) {
        console.log('Am innnnnnnn')
        this.orderForm.value['price'] = item.price
        this.gasPrice = item.price
      }
    }
  }

  onPayment() {
    this.navCtrl.push('PaymentPage', {})
  }

}
