import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {GeoLocationServiceProvider} from "../../providers/geo-location-service/geo-location-service";
import {SupplierServiceProvider} from "../../providers/supplier-service/supplier-service";
import {AgentServiceProvider} from "../../providers/agent-service/agent-service";
import {CustomerServiceProvider} from "../../providers/customer-service/customer-service";
import {TabsPage} from "../tabs/tabs";
import 'rxjs/add/operator/finally';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  userAs = 'customer';
  locationName: any;
  spinner: boolean = false;
  registerCustomer = {name: '', phone: '', email: ''};
  registerAgent = {name: '', phone: '', location: '', email: '', supplier_id: '', latitude: '', longitude: ''};
  registerSupplier = {name: '', phone: '', location: '', email: '', latitude: '', longitude: ''};
  suppliers: any;
  user: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _geoLocationService: GeoLocationServiceProvider,
              public alertCtrl: AlertController,
              public _supplierService: SupplierServiceProvider,
              public _agentService: AgentServiceProvider,
              public _customerService: CustomerServiceProvider) {
    this.user = navParams.get('user');

  }

  ionViewDidLoad() {
    this.getAllSuppliers(true);
  }

  doRefresh(refresher) {
    this.getAllSuppliers(false);
    setTimeout(() => {

      refresher.complete();
    }, 2000);
  }

  onAgentLocation(): void {
    this._geoLocationService.setCurrentLocation();
    let prompt = this.alertCtrl.create({
      title: 'Agent\'s Location',
      message: "Enter a name for this location",
      inputs: [
        {
          name: 'title',
          placeholder: 'Location Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {


            this.registerAgent['location'] = this.locationName = data.title;

            let info = this._geoLocationService.getLocationInfo()
            this.registerAgent['latitude'] = info['latitude'];
            this.registerAgent['longitude'] = info['longitude'];
            console.log(this.registerAgent)
          }
        }
      ]
    });
    prompt.present();

  }


  onSupplierLocation(): void {
    this._geoLocationService.setCurrentLocation();
    let prompt = this.alertCtrl.create({
      title: 'Supplier\'s Location',
      message: "Enter a name for this location",
      inputs: [
        {
          name: 'title',
          placeholder: 'Location Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {


            this.registerSupplier['location'] = this.locationName = data.title;

            let info = this._geoLocationService.getLocationInfo()
            this.registerSupplier['latitude'] = info['latitude'];
            this.registerSupplier['longitude'] = info['longitude'];
            console.log(this.registerSupplier)
          }
        }
      ]
    });
    prompt.present();

  }

  addCustomer() {
    console.log('Customer');
    console.log(this.registerCustomer)
    this._customerService.addCustomer(this.registerCustomer, this.user.id)
      .finally(() => {
        this.spinner = false;
      })
      .subscribe(customer => {
          this.navCtrl.setRoot(TabsPage);

          //current agent $ Supplier == null
          this._supplierService.currentSupplier = null;
          this._agentService.currentAgent = null;

        },
        error => {
          this.spinner = false;
          this.showPopup("Error", error);
        })
  }

  addAgent() {
    this._agentService.addAgent(this.registerAgent, this.user.id)
      .finally(() => {
        this.spinner = false;
      })
      .subscribe(agent => {
          this.navCtrl.setRoot(TabsPage);

          //current customer $ Supplier == null
          this._supplierService.currentSupplier = null;
          this._customerService.currentCustomer = null;

        },
        error => {
          this.spinner = false;
          this.showPopup("Error", error);
        })
  }

  addSupplier() {
    this._supplierService.addSupplier(this.registerSupplier, this.user.id)
      .finally(() => {
        this.spinner = false;
      })
      .subscribe(supplier => {
          this.navCtrl.setRoot(TabsPage);

          //current customer $ agent == null
          this._agentService.currentAgent = null
          this._customerService.currentCustomer = null

        },
        error => {
          this.spinner = false;
          this.showPopup("Error", error);
        })
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }


  //getters
  getAllSuppliers(status) {
    if (status)
      this.spinner = true;
    this._supplierService.getSuppliers()
      .finally(() => {
        this.spinner = false;
      })
      .subscribe(suppliers => {
        this.suppliers = suppliers;
      })
  }
}
