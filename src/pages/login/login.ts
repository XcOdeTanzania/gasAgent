import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder} from "@angular/forms";
import {User} from "../../interface/user.interface";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/finally';
import {TabsPage} from "../tabs/tabs";


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  spinner: boolean = false;
  registerCredentials = {email: '', password: ''};

  //user..
  user: User[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              public loadingCtrl: LoadingController,
              private _authService: AuthServiceProvider,
              private alertCtrl: AlertController,
              private storage: Storage
  ) {
  }

  ionViewWillEnter() {

  }

  public login() {
    this.showLoading()
    this._authService.login(this.registerCredentials)
      .finally(() => {
        this.spinner = false;
      })
      .subscribe(login => {

        if (login.status) {
          this.spinner = false;
          this.saveData(this._authService.getUserInfo());
          this.navCtrl.setRoot(TabsPage);
        } else {
          this.spinner = false;
          if (login.errors.email) {
            this.showPopup("Error", login.errors.email);
          } else if (login.errors.password) {
            this.showPopup("Error", login.errors.password);
          }
          else if (login.errors.user) {
            this.showPopup("Error", login.errors.user);
          }
          else {
            let err = login.errors.email + '\n' + login.errors.password
            this.showPopup("Error", err);
          }

        }
      });
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }

  showLoading() {
    this.spinner = true;
  }

  showError(text) {
    this.spinner = false;

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  saveData(userInfo) {
    this.storage.set('isLogin', true);
    this.storage.set('userInfo', userInfo);
  }

  signup() {

    this.navCtrl.push('SignupPage');
  }

  resetpassword() {
    this.navCtrl.push('ResetPasswordPage');
  }

}
