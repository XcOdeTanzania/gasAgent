import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  spinner: boolean = false;
  registerCredentials = {name: '', email: '', password: ''};

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private _authService: AuthServiceProvider,
    private alertCtrl: AlertController,
    private toast: ToastController
  ) {

  }


  signUp() {
    this.showLoading();
    this._authService.register(this.registerCredentials).subscribe(signup => {
        this.spinner = false;
        if (signup.status) {
          this.navCtrl.setRoot('RegisterPage', {
            user: signup.user
          });
          this.toast.create({
            message: `User created successfully`,
            duration: 3000
          }).present();
        } else {
          this.spinner = false;
          this.showPopup("Error", signup.errors.email);
        }

      },
      error => {
        this.spinner = false;
        this.showPopup("Error", "Unprocessed entry");
      });
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }

  showLoading() {
    this.spinner = true;
  }
}
