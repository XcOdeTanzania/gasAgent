import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  tigo: boolean = false;
  voda: boolean = false;
  airtel: boolean = false;
  tigoForm: FormGroup;
  vodaForm: FormGroup;
  airtelForm: FormGroup;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public navParams: NavParams) {
    this.tigoForm = formBuilder.group({
      phone: ['', Validators.required],
      amount: ['', Validators.required],
      pin: ['', Validators.required]

    })
    this.vodaForm = formBuilder.group({
      phone: ['', Validators.required],
      amount: ['', Validators.required],
      pin: ['', Validators.required]

    })
    this.airtelForm = formBuilder.group({
      phone: ['', Validators.required],
      amount: ['', Validators.required],
      pin: ['', Validators.required]

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  onTigo() {
    this.tigo = true;
    this.voda = false;
    this.airtel = false;
  }

  onVoda() {
    this.voda = true;
    this.tigo = false;
    this.airtel = false;
  }

  onAirtel() {
    this.airtel = true;
    this.voda = false;
    this.tigo = false;
  }

  paymentTransaction() {
    this.showPrompt();
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Payment',
      message: "Would you like to proceed with this transaction??",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: data => {
            this.presentToast('Transaction was successful')
          }
        }
      ]
    });
    prompt.present();
  }


  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
}
